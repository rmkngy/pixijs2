import * as PIXI from 'pixi.js'
import {TweenMax, Power2, TimelineLite} from 'gsap/TweenMax'


export default function() {
	let raf
	let currentIndex = 0
	let container = new PIXI.Container()
	let disp = 'images/displacement.jpg'
	let displacementSprite = PIXI.Sprite.fromImage(disp)
	displacementSprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT
	let displacementFilter = new PIXI.filters.DisplacementFilter(displacementSprite)
	let texts = ['Gino', 'Genoveffa', 'Giorgio', 'Gianna', 'Gianmarco']
	let sprites = ['images/img1.jpg', 'images/img2.jpg', 'images/img3.jpg', 'images/img4.jpg', 'images/img5.jpg']
	let app = new PIXI.Application({
		width: window.innerWidth,
		height: window.innerHeight
	})
	app.renderer.autoResize = true
	document.body.appendChild(app.view)

	displacementSprite.anchor.set(0.5)
	displacementSprite.scale.x = 2
	displacementSprite.scale.y = 2

	displacementFilter.scale.x = 0
	displacementFilter.scale.y = 0

	app.stage.addChild(container)
	app.stage.addChild(displacementSprite)
	container.filters = [displacementFilter]

	function rotateSprite() {
        displacementSprite.rotation += 0.002;
        raf = requestAnimationFrame( rotateSprite );
      }

	for (var i = 0; i < sprites.length; i++) {
		var background = PIXI.Sprite.fromImage(sprites[i])


		background.width = app.renderer.width
		background.height = app.renderer.height

		if (texts) {
			var textStyle = new PIXI.TextStyle({
				fill: '#f5f5f5',
				fontFamily: "Helvetica",
				fontVariant: "small-caps",
				fontSize: 100,
				fontWeight: 400
			})

			var text = new PIXI.Text(texts[i], textStyle)
			background.addChild(text)

			text.anchor.set(0.5)
			text.x = background.width / 2
			text.y = background.height
		}

		if(i !== 0) {
			TweenMax.set(background, {alpha: 0})
		}
		container.addChild(background)
	}
	let isPlaying = false
	let slideImages = container.children

	let moveSlider = function(newIndex) {
		isPlaying = true

		var baseTimeline = new TimelineLite({ onComplete: function() {
			currentIndex = newIndex
			isPlaying = false
		}})

		rotateSprite()
		baseTimeline
			.to(displacementFilter.scale, 0.5, {x: 2000, y: 300})
			.to(slideImages[currentIndex], 0.5, {alpha: 0})
			.to(slideImages[newIndex], 0.5, {alpha:1}, '-=0.4')
			.to(displacementFilter.scale, 0.5, {x: 0, y: 0, onComplete: function() {
				cancelAnimationFrame(raf)
			}})
	}

	let nav = document.querySelectorAll('.scene-nav')
	nav.forEach((el) => {
		el.addEventListener('click', (e) => {
			if(isPlaying) {
				return false
			}
			if (e.target.getAttribute('data-nav') === 'next') {
				if(currentIndex >=0 && currentIndex < slideImages.length - 1) {
					moveSlider(currentIndex + 1)
				}else {
					moveSlider(0)
				}
			}

			if (e.target.getAttribute('data-nav') === 'previous') {
				if(currentIndex >= 1 && currentIndex < slideImages.length) {
					moveSlider(currentIndex - 1)
				} else {
					moveSlider(slideImages.length - 1)
				}
			}

		})
	})
}
