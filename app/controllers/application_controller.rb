class ApplicationController < ActionController::Base
	before_action :set_meta

	def set_meta
      # Default metatags, you can edit them in your actions, see README.md
      # For other options, go to https://github.com/kpumuk/meta-tags
    	set_meta_tags og: {
        		title:        :title,
        		description:  :description,
        		type:         'website',
        		url:          :canonical,
        		image:        CONFIG['fb_share_image']
        	},
       		title: "Sample App",
       		description: "Sample App description",
       		canonical: request.original_url
  end

end
