class UtilsController < ApplicationController

	def robots
		render "utils/robots.txt", layout: false, content_type: "text/plain"
	end

end
