# RAILS 5 DUMMY APP


## HOW TO

- Clone this app
- run: $ bundle
- run: $ yarn
- change databases info in app/database.yml
- rename it with: $ rails g rename:app_to NewAwesomeName
- delete local .git folder and launch: $ git init


## ROBOTS.TXT
You can enable or disable it in config.yml


## CLI Commands
'''
// Run the server with local binding to 0.0.0.0
$ yarn serve

// Run webpack with watch for dev
$ yarn webpack_dev

// Run webpack with watch for dev
$ yarn webpack_prod
'''


# Heroku

**ENV variables**
NODE_ENV=production

**Buildpacks**
The order is important
heroku/nodejs
heroku/ruby

With this buildpacks, heroku will compile all your assets with webpack_prod while building the app


## Global Variables

- Put all your variables in approot/config.yml
- Access them with
'''
CONFIG['your_key']
# note that the key is a string, not a symbol
'''


## Google Analytics

- Set up your code in approot/config.yml


## Meta Tags

- You can change default meta tags in application_controller.erb
- If you need to change meta tags in a specific controller->action, change this global parameters
	@page_title 
	@page_description
- For other info https://github.com/kpumuk/meta-tags


## Favicons

- Put all in /public/favicons


## Images

- Put images in /public/images


## Javascript ES2015 and React + JSX

- This application uses webpack
- install local modules with : $ yarn 

## ASSETS WORKFLOW : !IMPORTANT

- If you don't follow this rules: **I will find you, and I will kill you**
- Assets are splitted in components, so EVERYTHING is a component.

**STYLESHEETS**

- Resets in settings/_resets.scss
- Components:  If you need some common styles, write it in components/_commons.scss
- Every imported file must start with an underscore for sass concatenation
- Put your static libraries in lib
- In settings you have everything you need
- Responsive: don't use media queries, but use it ( you can find all breakpoints in settings/_params.scss ):

```
@include break( medium ) {
	...
}

@include min-break( medium ) { // it starts from breakpoint + 1px -> Example: @media ( min-width: 321px )
	...
}
```
- You can define all colors in settings/_params.scss
- When you define a color, this app generates two classes for text and background color
```
.c-colorname {
	color: color(colorname);
}
.bg-colorname {
	background-color: color(colorname);
}
```

**PADDING, MARGIN AND SPACES**
- Padding is 8px, you can call it with:
```
padding: padding(); // it will be 8px
margin: padding(2); // it will be 8px * 2
```

**FONTS**
```
@include fontBreak()
// go to typo.scss for an example
```

**JAVASCRIPTS**

- Start webpack with: $ yarn webpack_dev
- Like stylesheets, everything is a component
- Put your static libraries in lib
- Put your utils in utils
- Follow this simple rules https://github.com/airbnb/javascript

