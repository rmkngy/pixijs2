const path = require('path')

const Paths = {}

Paths.base = path.resolve(__dirname, "../app/assets")
Paths.js    = path.join(Paths.base, "/javascripts" )
Paths.entry = {
		app: [ path.join(Paths.js, "/app.jsx" ) ],
	}
Paths.dist  = path.join(Paths.js, "dist")
Paths.utils = path.join(Paths.js, "lib/utils.js")

module.exports = Paths