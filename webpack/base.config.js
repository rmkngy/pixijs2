const webpack  = require('webpack')
    , Paths    = require('./paths')

module.exports = {
    entry: Paths.entry,
    mode: "development",
    resolve: {
        extensions: ['.js', '.frag', '.vert', '.jsx', '.ts', '.tsx'],
        alias: {
            utils: Paths.utils,
        },
    },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                include: Paths.js,
                exclude: [ /node_modules/, Paths.dist ],
            },
            {
                test: /\.(glsl|frag|vert)$/,
                loader: 'raw-loader',
                exclude: [ /node_modules/, Paths.dist]
            },
            {
                test: /\.(glsl|frag|vert)$/,
                loader: 'glslify-loader',
                exclude: [ /node_modules/, Paths.dist]
            },
        ],
    },
    output: {
        filename: '[name].bundle.js',
        path: Paths.dist,
    },
}

// ADD babel polifyll to all namespaces
for ( var key in module.exports.entry ) {
    module.exports.entry[key].unshift('whatwg-fetch')
    module.exports.entry[key].unshift('babel-polyfill')
}
