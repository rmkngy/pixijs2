const merge 	 = require('webpack-merge') 
const baseConfig = require('./base.config.js')

module.exports = merge(baseConfig, {

	devtool: "source-map",
	watch: false,

	plugins: [
		new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            compress: {
               screw_ie8: true,
               warnings: false
            }
        })
	]
	
})