const merge 	 = require('webpack-merge') 
const Notifier   = require('webpack-build-notifier')
const baseConfig = require('./base.config.js')

module.exports = merge(baseConfig, {

	devtool: "eval",
	watch: true,

	plugins: [
		new Notifier({ successSound: false })
	]
	
})